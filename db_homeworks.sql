-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-03-2022 a las 21:57:24
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_homeworks`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(1, '2022-03-01-174549', 'App\\Database\\Migrations\\Status', 'default', 'App', 1646157161, 1),
(2, '2022-03-01-174618', 'App\\Database\\Migrations\\Work', 'default', 'App', 1646157161, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE `status` (
  `id` tinyint(4) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`id`, `name`) VALUES
(1, 'Pending'),
(2, 'In process'),
(3, 'Finished'),
(4, 'Cancelled');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `work`
--

CREATE TABLE `work` (
  `id` tinyint(4) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `status_id` tinyint(4) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `modification_date` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `finish_date` datetime NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `deleted_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `work`
--

INSERT INTO `work` (`id`, `name`, `description`, `status_id`, `created_date`, `modification_date`, `finish_date`, `deleted`, `deleted_at`) VALUES
(1, 'Test1', 'Test1', 1, '2022-03-01 12:02:41', '2022-03-01 12:02:41', '2022-03-01 19:01:29', 0, '2022-03-01 12:16:15'),
(2, 'Test2', 'Test2', 2, '2022-03-01 12:02:41', '2022-03-02 11:44:26', '2022-03-01 12:02:41', 0, '2022-03-01 12:16:15'),
(3, 'Test3', 'Test3', 3, '2022-03-01 12:02:41', '2022-03-02 11:44:22', '2022-03-01 12:02:41', 0, '2022-03-01 12:16:15'),
(4, 'Test4', 'Test4', 1, '2022-03-01 12:02:41', '2022-03-02 13:43:33', '0000-00-00 00:00:00', 0, '2022-03-01 12:16:15'),
(5, 'Test delete', 'Test4', 4, '2022-03-01 12:41:14', '2022-03-02 11:36:03', '2022-03-01 12:41:14', 0, '2022-03-01 12:41:14'),
(7, 'we', 'de', 2, '2022-03-02 14:01:59', '2022-03-02 14:46:56', '0000-00-00 00:00:00', 0, '2022-03-02 14:01:59'),
(8, 'kdkd', 'iiid', 4, '2022-03-02 14:03:36', '2022-03-02 14:03:36', '2022-03-02 14:03:22', 0, '2022-03-02 14:03:36'),
(9, 'kos', 'iid', 1, '2022-03-02 14:03:58', '2022-03-02 14:03:58', '0000-00-00 00:00:00', 0, '2022-03-02 14:03:58');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `work`
--
ALTER TABLE `work`
  ADD PRIMARY KEY (`id`),
  ADD KEY `work_status_id_foreign` (`status_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `status`
--
ALTER TABLE `status`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `work`
--
ALTER TABLE `work`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `work`
--
ALTER TABLE `work`
  ADD CONSTRAINT `work_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

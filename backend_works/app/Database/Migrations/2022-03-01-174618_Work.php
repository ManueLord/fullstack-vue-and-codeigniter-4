<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Work extends Migration
{
    public function up()
    {
        // $this->db->disableForeignKeyChecks();

        $this->forge->addField([
            'id' => [
                'type' => 'TINYINT',
                'auto_increment' => true
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => 200,
            ],
            'description' => [
                'type' => 'TEXT'
            ],
            'status_id' => [
                'type' => 'TINYINT', 
                'constraint' => 4,
                'null' => true
            ],
            'created_date DATETIME default current_timestamp',
            'modification_date DATETIME default current_timestamp on update current_timestamp',
            // 'created_date' => [
            //     'type' => 'TIMESTAMP'
            // ],
            // 'modification_date' => [
            //     'type' => 'TIMESTAMP'
            // ],
            'finish_date' => [
                'type' => 'DATETIME'
            ],
            'deleted' => [
                'type' => 'INT'
            ]
        ]);
        
        $this->forge->addKey('id', TRUE);
        $this->forge->addForeignKey('status_id', 'status', 'id', 'CASCADE', 'CASCADE');
        $this->forge->createTable('work');

        // $this->db->enableForeignKeyChecks();
    }

    public function down()
    {
        $this->forge->dropTable('work');
    }
}

<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\HTTP\RequestTrait;
use App\Models\StatusModel;
use App\Models\WorkModel;

class Homework extends ResourceController
{
    use RequestTrait;
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    public function index()
    { 
        $model = new WorkModel();
        $works = $model->findAll();
        if (!$works) return $this->failNotFound('No found datas');
        return $this->respond($works);    
    }

    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function show($id = null)
    {
        $model = new WorkModel();
        $works = $model->find(['id' => $id]);
        if (!$works) return $this->failNotFound('No found datas');
        return $this->respond($works);
    }

    /**
     * Return a new resource object, with default properties
     *
     * @return mixed
     */
    public function new()
    {
        //
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        $json = $this->request->getJSON();
        $data = [
            'name' => $json->name,
            'description' => $json->description,
            'status_id' => $json->status_id,
            'finish_date' => $json->finish_date
        ];
        $model = new WorkModel();
        $work = $model->insert($data);
        if(!$work) return $this->fail('Error from insert work   ', 400);
        return $this->respondCreated($work);
    }

    /**
     * Return the editable properties of a resource object
     *
     * @return mixed
     */
    public function edit($id = null)
    {
        //
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        $json = $this->request->getJSON();
        $data = [
            'name' => $json->name,
            'description' => $json->description,
            'status_id' => $json->status_id,
            'finish_date' => $json->finish_date
        ];
        $model = new WorkModel();
        $findWork = $model->find(['id' => $id]);
        if(!$findWork) return $this->fail('No found work', 404);
        $work = $model->update($id, $data);
        if(!$work) return $this->fail('Error from delete work', 400);
        return $this->respondUpdated($work);
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        $model = new WorkModel();
        $findWork = $model->find(['id' => $id]);
        if(!$findWork) return $this->fail('No found work', 404);
        $work = $model->delete($id);
        if(!$work) return $this->fail('Error from update work', 400);
        return $this->respondDeleted($work);
    }
}

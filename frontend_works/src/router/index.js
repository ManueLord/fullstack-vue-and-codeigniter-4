import { createRouter, createWebHistory } from 'vue-router'
import HomeworkList from '../views/HomeworkList'
import AddHomework from '../views/AddHomework'
import EditHomework from '../views/EditHomework'

const routes = [
  {
    path: '/',
    name: 'HomeworkList',
    component: HomeworkList
  },
  {
    path: '/add',
    name: 'AddHomework',
    component: AddHomework
  },
  {
    path: '/edit/:id',
    name: 'EditHomework',
    component: EditHomework
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router

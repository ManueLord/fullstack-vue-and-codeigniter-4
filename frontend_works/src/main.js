import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { createApp } from 'vue';
import router from './router';
import axios from 'axios';
import App from './App.vue';

axios.defaults.baseURL = 'http://localhost:8080/'

library.add(fas);

createApp(App).use(router).component('fa', FontAwesomeIcon).mount('#app')
